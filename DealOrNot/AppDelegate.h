//
//  AppDelegate.h
//  DealOrNot
//
//  Created by Andrew Craze on 5/10/14.
//  Copyright (c) 2014 Codeswell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
