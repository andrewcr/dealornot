//
//  main.m
//  DealOrNot
//
//  Created by Andrew Craze on 5/10/14.
//  Copyright (c) 2014 Codeswell. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
