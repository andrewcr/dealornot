//
//  ViewController.m
//  DealOrNot
//
//  Created by Andrew Craze on 5/10/14.
//  Copyright (c) 2014 Codeswell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray* buttons;
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;

@end

@implementation ViewController

- (void)viewDidAppear:(BOOL)animated {
    [self showValueOfDeal];
}


- (void)showValueOfDeal {
    NSInteger totalValue = 0;
    NSInteger caseCount = 0;
    float valueOfDeal = 0.;
    
    for (UIButton* btn in self.buttons) {
        if (btn.isEnabled) {
            caseCount++;
            totalValue += [btn.titleLabel.text integerValue];
        }
    }
    
    if (caseCount > 0) {
        valueOfDeal = totalValue/(float)caseCount;
    }
    self.valueLabel.text = [NSString stringWithFormat:@"$ %6.2f", valueOfDeal];
}

- (IBAction)buttonTapped:(UIButton*)btn {
    btn.backgroundColor = [UIColor redColor];
    btn.enabled = NO;
    [self showValueOfDeal];
}

- (IBAction)resetButtonTapped:(UIButton*)dummy {
    for (UIButton* btn in self.buttons) {
        btn.enabled = YES;
        btn.backgroundColor = [UIColor lightGrayColor];
    }
    [self showValueOfDeal];
}

@end
